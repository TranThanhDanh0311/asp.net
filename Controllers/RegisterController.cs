﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using WebApplication1.Models;
using MySql.Data.MySqlClient;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public RegisterController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public JsonResult Get()
        {
            string query = @"
                            select * from
                            dangnhap
                            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("EmployeeAppCon");
            MySqlDataReader myReader;
            using (MySqlConnection myCon = new MySqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (MySqlCommand myCommand = new MySqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }
        [HttpPost]
        public JsonResult Post(Register reg)
        {
            string query = @"
                           insert into dangnhap
                            (Username,password,HoTen,SDT,diaChi,cRole,Email,picture)
                           values (@Username,@password,@HoTen,@SDT,@diaChi,@cRole,@Email,@picture)
                            ";

            DataTable table = new DataTable();
             string sqlDataSource = _configuration.GetConnectionString("EmployeeAppCon");
            MySqlDataReader myReader;
             using (MySqlConnection myCon = new MySqlConnection(sqlDataSource))
             {
                 myCon.Open();
                 using (MySqlCommand myCommand = new MySqlCommand(query, myCon))
                 {
                     myCommand.Parameters.AddWithValue("@Username", reg.Username);
                    myCommand.Parameters.AddWithValue("@password", reg.password);
                    myCommand.Parameters.AddWithValue("@HoTen", reg.HoTen);
                    myCommand.Parameters.AddWithValue("@SDT", reg.SDT);
                    myCommand.Parameters.AddWithValue("@diaChi", reg.diaChi);
                    myCommand.Parameters.AddWithValue("@cRole", 1);
                    myCommand.Parameters.AddWithValue("@Email", reg.Email);
                    myCommand.Parameters.AddWithValue("@picture", reg.picture);
                    myReader = myCommand.ExecuteReader();;
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
             } 

            return new JsonResult("add success");
        }
    }
}
