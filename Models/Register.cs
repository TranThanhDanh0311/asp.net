﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Register
    {
        public string Username { get; set; }

        public string password { get; set; }
        public string HoTen { get; set; }
        public string SDT { get; set; }
        public string diaChi { get; set; }
        public string cRole { get; set; }
        public string Email { get; set; }
        public string picture { get; set; }
    }
}
